function [ d ] = demand(p,q)
% Learing by duopoly doing 
% Logit demand
% para is a structure containg all the parameters of the model. 
global L rho kappa l v delta beta 
one = ones(L,1);
denom = one+exp(v*one - p)+exp(v*one - q);
d1 = exp(v*one - p)./denom ; 
d2 = exp(v*one - q)./denom ; 
d0 = one-d1-d2; 
d = [d0';d1';d2'];
end

