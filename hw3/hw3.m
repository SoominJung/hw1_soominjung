

% Econ512 hw3 written by Soomin Jung
% Extramarital affairs 
clear all;
clc;
diary hw3log.out

load('hw3.mat') 

% y = # of affairs in the past year
% x = intercept age #of years married religiousness(1-5) occupation(1-7)
% self-rating of marriage(1-5) 
% log likelihood function is named objhw3 

[n,k]=size(X); 
initialb=zeros(k,1); 

%% 1. MLE with FMINUNC w/o analytical derivative 
option1 = optimset('Display','off','GradObj','off',...
    'Algorithm','trust-region-reflective','HessUpdate','bfgs');
[b1,fval1]=fminunc(@(b) objhw3(X,y,b), initialb, option1);

%% 2. MLE with Fminunc w/ derivative 
option2 = optimset('Display','off','GradObj','on',...
    'Algorithm','trust-region-reflective','HessUpdate','bfgs');
% Here you lack increasing 'TolFun' parameter for algo to converge to the
% same point, difference in beta should warn you

%options = optimset('fminunc','Display','iter','GradObj','on','Algorithm','quasi-newton','HessUpdate','bfgs');
[b2,fval2]=fminunc(@(b) objhw3(X,y,b), initialb, option2);
disp('Fminunc w/o derivative')
b1
fval1
disp('Fminunc w/ derivative')
b2
fval2 

%% 3.MLE with Nealder mead 
optset('neldmead','maxit',10000);
% correct, default number of iteration is not enough
[b3, ~]=neldmead(@(b) -objhw3(X,y,b), initialb); 
fval3=objhw3(X,y,b3); 
disp('Nealder mead')
b3
fval3

%% 4. BHHH
maxit=1000; 
tol=0.0001; 
% Here also you need to increase precision
i=1; 
diff=1; 
btemp=initialb; 
tic
while diff>tol && i<maxit
    [~,s,h]=objhw3(X,y,btemp); % s has (-), 1x6 matrix 
    bupdate=btemp-inv(h)*s'; 
    diff=sum(abs(btemp-bupdate)); 
    btemp=bupdate; 
    i=i+1; 
end
toc
b4=bupdate;
fval4=objhw3(X,y,b4);
b4
fval4
i

[~,~,h0]=objhw3(X,y,initialb); 
b0_eigenv=eig(h0); 
[~,~,h1]=objhw3(X,y,b4); 
b4_eigenv=eig(h0); 

disp('Eigen value of hessian 1)initial guess 2) stopping value')
b0_eigenv
b4_eigenv
%% 5. NLLS 
btemp=initialb; 
iter=1;
d=1; 

while d>tol && iter<maxit; 
    iter=iter+1; 
    J=-diag(exp(X*btemp))*X; 
    f=y-exp(X*btemp); 
    bnew=btemp-((J'*J)^(-1)*J'*f); 
    d=sum(abs(bnew-btemp)); 
    btemp=bnew;    
end 

bnlls=bnew; 
bnlls
iter

diary off 