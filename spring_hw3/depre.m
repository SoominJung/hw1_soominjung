function [ w1n, w2n ] = depre( q1,q2,w1,w2 )
% Learing by duopoly doing 
% depreciation 

global L rho kappa l v delta beta 

shock1 = rand(1,1) ; 
shock2 = rand(1,1) ; 
        
c1 = (1-(1-delta)^w1);  
c2 = (1-(1-delta)^w2);

if q1 == 1 
    if w1 == L 
       w1n = w1; 
    elseif shock1 < c1
       w1n = w1 ;
    else 
        w1n = w1+1; 
    end
    
    if w2 == 1
       w2n = w2;
    elseif shock2 < c2
        w2n = w2-1 ; 
    else 
        w2n = w2; 
    end 
end 

if q2 == 1 
    if w2 == L 
       w2n = w2; 
    elseif shock2 < c2
       w2n = w2 ;
    else 
        w2n = w2+1; 
    end
    
    if w1 == 1
       w1n = w1;
    elseif shock1 < c1
        w1n = w1-1 ; 
    else 
        w1n = w1; 
    end 
end 

if q1== 0 && q2 == 0 
    if w1 == 1 
        w1n = w1 ; 
    elseif shock1 < c1 
        w1n = w1 - 1;
    else 
        w1n = w1 ; 
    end
    
    if w2 == 1 
        w2n = w2 ; 
    elseif shock2 < c2 
        w2n = w2 - 1;
    else 
        w2n = w2 ; 
    end
end

