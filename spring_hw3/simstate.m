function [ prob ] = simstate( N,simT,P )

global L rho kappa l v delta beta 

    %simW = zeros(N,2) ;
    prob = zeros(L,L) ; 

for j = 1:N
        w1 = 1;     % initial state of firm 1
        w2 = 1;     % initial state of firm 2
        
    for i = 1 : simT

        p1 = P(w1,w2) ; % price 1 given (w1,w2) 
        p2 = P(w2,w1) ; % price 2 given (w1,w2)
    
        d = demand(p1,p2) ; % d(1) : q1=q2=0, d(2) : q1=1,q2=0, d(3) : q1=0,q2=1
    
        % Consumer choice 
    
        shock = rand(1,1) ; 
    
        q1 = 0 ;
        q2 = 0 ;
    
        if shock > d(1) 
            if shock <d(1)+d(2)
                q1 = 1;
            else
                q2 = 1;
            end
        end
    
        % Depreciation 
       [w1,w2] = depre(q1,q2,w1,w2); 
    
   end 

       %simW(j,1)= w1 ;
       %simW(j,2)= w2 ; 
       prob(w1,w2) = prob(w1,w2)+1/N ; 
end 


end

