function [ fval ] = sysfoc( p ,v )
% % firm A,B,C's foc 
% denom=(1+exp(-1-p(1))+exp(-1-p(2))+exp(-1-p(3)));
% fa=p(1)*(1-exp(-1-p(1))/denom)-1;
% fb=p(2)*(1-exp(-1-p(2))/denom)-1;
% fc=p(3)*(1-exp(-1-p(3))/denom)-1;
% fval=[fa;fb;fc]; 

denom=(1+exp(v(1)-p(1))+exp((v(2)-p(2))+exp(v(3)-p(3))));
fa=p(1)*(1-exp(v(1)-p(1))/denom)-1;
fb=p(2)*(1-exp(v(2)-p(2))/denom)-1;
fc=p(3)*(1-exp(v(3)-p(3))/denom)-1;
fval=[fa;fb;fc]; 
end

