%parameter 

L = 30 ; % number of state (knowledge level)
rho = 0.85 ; % cost parameter 
kappa = 10 ; % minimum knowledge 
l = 15 ; % cost flatten out 
v = 10 ; % quality of good 
delta = 0.03 ; % depreciation probability 
beta = 1/1.05 ; % discounting factor 
