function [ foc ] = foc( p,q,c,w0,w1,w2 )
% Learing by duopoly doing 
% foc
% fix firm 1's state as w(i)
% p is firm 1's price firm 2's state given) 
% q is frim 2's price (Lx1) for each state w 

global L rho kappa l v delta beta 

one = ones(L,1) ; 
d = demand(p,q);
margin = p - c*one ;

foc = one-(one-d(2,:)').*margin-beta*w1+beta*(d(1,:)'.*w0+d(2,:)'.*w1+d(3,:)'.*w2); 

end

