function [ c ] = cost( para )
% Learing by duopoly doing 
% production cost
% w is a knowledge level (state)
% para is a structure containg all the parameters of the model. 
c = zeros(para.L,1) ; 
for w = 1: para.L
eta = log(para.rho) / log(2) ; 
if w <= para.l  
    c(w) = para.kappa * (w^eta) ; 
else 
    c(w) = para.kappa * (para.l ^ eta );
end     
end
end 

