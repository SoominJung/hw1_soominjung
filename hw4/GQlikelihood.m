function [ obj ] = GQlikelihood(x,y,z,beta,gamma,sigma,p)
% Likelihood using Gaussian Quadrature integration 
    
    [grid,weight]=qnwnorm(p,beta,sigma); 
    like=0; 
    
    for i=1:p; 
        e=grid(i)*x+gamma*z;
        fe = 1./(1+exp(-e));
        l_1=y.*log(fe);
        l_0=(1-y).*log(1-fe);
        l=min(l_1,l_0);         % to prevent -inf 
        like=sum(sum(l))*weight(i)+like; 
    end
    
        obj=-like;
 
end
