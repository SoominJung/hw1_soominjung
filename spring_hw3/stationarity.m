global L rho kappa l v delta beta eta 

L = 30 ; % number of state (knowledge level)
rho = 0.85 ; % cost parameter 
kappa = 10 ; % minimum knowledge 
l = 15 ; % cost flatten out 
v = 10 ; % quality of good 
delta = 0.03 ; % depreciation probability 
beta = 1/1.05 ; % discounting factor 
eta = log(rho) / log(2) ; 


prob = zeros(L,L) ; 
prob(1,1) = 1 ;
prob(1,2) = 0;
probu = zeros(L,L) ; 
diff = 1 ; 
tol = 10^-4 ; 
iter = 0; 

while diff > tol && iter < 1000  

    for i = 1 : L 
    
        for j = 1 : L 
        
            p = 0 ;
        
        
            if i~=1 && i~=L && j~=1 && j~=L  % i,j neq 1,30                   
                w1 = i+1 ;        
                w2 = j+1 ;        
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );
                p = p + prob(w1,w2)*d0*delta1*delta2 ;
                
                w1 = i+1 ;       
                w2 = j ;        
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );        
                p = p + prob(w1,w2)*(d0*delta1*(1-delta2)+d2*delta1*delta2);          
        
                w1 = i+1 ;        
                w2 = j-1 ;        
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );        
                p = p + prob(w1,w2)*(d2*delta1*(1-delta2));        
        
                w1 = i ;        
                w2 = j+1 ;        
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );       
                p = p + prob(w1,w2)*(d0*(1-delta1)*delta2+d1*delta1*delta2);
        
        
                w1 = i ;        
                w2 = j ;        
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );        
                p = p + prob(w1,w2)*(d0*(1-delta1)*(1-delta2)+d1*delta1*(1-delta2)+...            
                    d2*(1-delta1)*delta2);                
        
                w1 = i ;         
                w2 = j-1 ;         
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );        
                p = p + prob(w1,w2)*(d2*(1-delta1)*(1-delta2));
        
        
                w1 = i-1 ;         
                w2 = j+1 ;         
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );        
                p = p + prob(w1,w2)*(d1*(1-delta1)*delta2);
        
        
                w1 = i-1 ;        
                w2 = j ;         
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );       
                p = p + prob(w1,w2)*(d1*(1-delta1)*(1-delta2)); 
        
        
            elseif i==1 && j==1 % if i,j = 1
                p = 0 ; 
                w1 = i+1 ;        
                w2 = j+1 ;        
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );        
                p = p + prob(w1,w2)*(d0*delta1*delta2) ; 
        
        
                w1 = i+1 ;
                w2 = j ;        
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );        
                p = p + prob(w1,w2)*(d0*delta1*1+d2*delta1*delta2);
        
        
                w1 = i ;        
                w2 = j+1 ;        
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );       
                p = p + prob(w1,w2)*(d0*1*delta2+d1*delta1*delta2);        
        
                w1 = i ;        
                w2 = j ;        
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );        
                p = p + prob(w1,w2)*(d0*1*1+d1*delta1*1+d2*1*delta2);
        
        
            elseif i==1 && j~=1 && j~=30
                p = 0 ; 
                w1 = i+1 ;       
                w2 = j+1 ;        
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );        
                p = p + prob(w1,w2)*(d0*delta1*delta2) ; 
        
        
                w1 = i+1 ;        
                w2 = j ;        
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );       
                p = p + prob(w1,w2)*(d0*delta1*(1-delta2)+d2*delta1*delta2);
        
        
                w1 = i+1 ;       
                w2 = j-1 ;        
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );        
                p = p + prob(w1,w2)*(d2*delta1*(1-delta2));
        
        
                w1 = i ;        
                w2 = j+1 ;        
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );        
                p = p + prob(w1,w2)*(d0*1*delta2+d1*delta1*delta2);
        
        
                w1 = i ;        
                w2 = j ;        
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );        
                p = p + prob(w1,w2)*(d0*1*(1-delta2)+d1*delta1*(1-delta2)+...
                    d2*1*delta2);
        
                w1 = i ;         
                w2 = j-1 ;         
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );        
                p = p + prob(w1,w2)*(d2*1*(1-delta2));
            
            elseif i==1 && j==L 
                p = 0 ; 
        w1 = i+1 ;
        w2 = j ;
        [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );
        p = p + prob(w1,w2)*(d0*delta1*(1-delta2)+d2*delta1*1);
        
        w1 = i+1 ;
        w2 = j-1 ;
        [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );
        p = p + prob(w1,w2)*(d2*delta1*(1-delta2));
        
        
        w1 = i ;
        w2 = j ;
        [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );
        p = p + prob(w1,w2)*(d0*1*(1-delta2)+d1*delta1*(1-delta2)+...
            d2*1*1);
        
        w1 = i ; 
        w2 = j-1 ; 
        [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );
        p = p + prob(w1,w2)*(d2*1*(1-delta2));
        
        
            elseif i==L && j==L    
       
                w1 = i ;
        
                w2 = j ;
        
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );
       
                p = p + prob(w1,w2)*(d0*(1-delta1)*(1-delta2)+d1*1*(1-delta2)+...
                    d2*(1-delta1)*1);
        
        w1 = i ; 
        w2 = j-1 ; 
        [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );
        p = p + prob(w1,w2)*(d2*(1-delta1)*(1-delta2));
        
        
        w1 = i-1 ;
        w2 = j ; 
        [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );
        p = p + prob(w1,w2)*(d1*(1-delta1)*(1-delta2));             
            
            elseif i~=1 && i~=L && j==1                 
                w1 = i+1 ;        
                w2 = j+1 ;        
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );
                p = p + prob(w1,w2)*d0*delta1*delta2 ;
                
                w1 = i+1 ;       
                w2 = j ;        
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );        
                p = p + prob(w1,w2)*(d0*delta1*1+d2*delta1*delta2);                          
        
                w1 = i ;        
                w2 = j+1 ;        
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );       
                p = p + prob(w1,w2)*(d0*(1-delta1)*delta2+d1*delta1*delta2);
        
        
                w1 = i ;        
                w2 = j ;        
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );        
                p = p + prob(w1,w2)*(d0*(1-delta1)*1+d1*delta1*1+...            
                    d2*(1-delta1)*delta2);                               
        
                w1 = i-1 ;         
                w2 = j+1 ;         
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );        
                p = p + prob(w1,w2)*(d1*(1-delta1)*delta2);
        
        
                w1 = i-1 ;        
                w2 = j ;         
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );       
                p = p + prob(w1,w2)*(d1*(1-delta1)*1); 
                
            elseif i==L && j==1   %%  
                p = 0 ;                 
                w1 = i ;        
                w2 = j+1 ;        
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );       
                p = p + prob(w1,w2)*(d0*(1-delta1)*delta2+d1*1*delta2);
        
        
                w1 = i ;        
                w2 = j ;        
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );        
                p = p + prob(w1,w2)*(d0*(1-delta1)*1+d1*1*1+...            
                    d2*(1-delta1)*delta2);       
        
                w1 = i-1 ;         
                w2 = j+1 ;         
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );        
                p = p + prob(w1,w2)*(d1*(1-delta1)*delta2);
        
        
                w1 = i-1 ;        
                w2 = j ;         
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );       
                p = p + prob(w1,w2)*(d1*(1-delta1)*1); 
            
            elseif i~=1 && i~=L && j==L      
                p = 0 ;                 
                w1 = i+1 ;       
                w2 = j ;        
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );        
                p = p + prob(w1,w2)*(d0*delta1*(1-delta2)+d2*delta1*1);          
        
                w1 = i+1 ;        
                w2 = j-1 ;        
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );        
                p = p + prob(w1,w2)*(d2*delta1*(1-delta2));        
        
                
                w1 = i ;        
                w2 = j ;        
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );        
                p = p + prob(w1,w2)*(d0*(1-delta1)*(1-delta2)+d1*delta1*(1-delta2)+...            
                    d2*(1-delta1)*1);                
        
                w1 = i ;         
                w2 = j-1 ;         
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );        
                p = p + prob(w1,w2)*(d2*(1-delta1)*(1-delta2));      
        
                
                w1 = i-1 ;        
                w2 = j ;         
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );       
                p = p + prob(w1,w2)*(d1*(1-delta1)*(1-delta2)); 
                
            else % i==L && j~=1 && j~=L    
                p = 0 ;                 
                w1 = i ;        
                w2 = j+1 ;        
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );       
                p = p + prob(w1,w2)*(d0*(1-delta1)+d1*1)*delta2;        
        
                w1 = i ;        
                w2 = j ;        
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );        
                p = p + prob(w1,w2)*(d0*(1-delta1)*(1-delta2)+d1*1*(1-delta2)+...            
                    d2*(1-delta1)*delta2);                
        
                w1 = i ;         
                w2 = j-1 ;         
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );        
                p = p + prob(w1,w2)*(d2*(1-delta1)*(1-delta2));
        
        
                w1 = i-1 ;         
                w2 = j+1 ;         
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );        
                p = p + prob(w1,w2)*(d1*(1-delta1)*delta2);
        
        
                w1 = i-1 ;        
                w2 = j ;         
                [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P );       
                p = p + prob(w1,w2)*(d1*(1-delta1)*(1-delta2)); 
        
            end
            
            probu(i,j) = p ;             
           % probu(j,i) = p ; % Prob matrix is symmetric 
    
        end
        
    end
        diff = probu - prob ; 
        diff = norm(diff) ;
        prob = probu ;
        iter = iter + 1 ;
        
end 
sum(sum(prob))
figure(6)
mesh(x,y,probu)
alpha(.5) 
title('Stationary distribution')
xlabel('w1'); ylabel('w2'); zlabel('Probability(w)')