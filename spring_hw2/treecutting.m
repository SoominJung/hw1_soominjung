% Econ 512 Spring hw 1
% Soomin Jung 

clear all;
close all;

% Parameter values

beta=0.95; % discount factor

Z = 21;               % grid number for shocks
% Z = 5 ; 

sigma = 0.1;         % std of error term in AR(1) process
mu = 0.5;             % p0 
ro = 0.5;             % AR(1) parameter 
 
 

% GRID FOR LUMBER STOCK 

% step 1: using Tauchen method to find the grid points for the tree price 

[prob,grid]=tauchen(Z,mu,ro,sigma);
%disp(['The dimensions of prob are ' num2str(size(prob)) ])
%disp(['The dimensions of grid are ' num2str(size(grid)) ])

x = 0:1:100 ;  % Stock of lumber, positive integer 
N = length(x); 
vinitial = zeros(N,Z); 
vrevised = zeros(N,Z);
decision = zeros(N,Z);

nextx=kron(ones(1,Z),x');
one=ones(N,1); 
%disp(['The stock of next period lumber are ' num2str(size(nextx)) ])


%iteration

diff=1;

while diff>0.001
    
    Ev=vinitial*prob';   % find the expected value of value function
      
    for i=1:N           % for each k, find the optimal decision rule
      cut = x(i)-x ;   
      revenue = cut'*grid ; 
      cut = max(cut, 0) ; % give 0 if x-nextx is negative 
      cost = cut'.^1.5*0.2 ; 
      cost = repmat(cost,1,Z) ; 
      profit = revenue - cost;  
     % disp(['The dimensions of ci are ' num2str(size(ci)) ])
      [vrevised(i,:),decision(i,:)]=max(profit+beta*Ev);
     % disp(['In the loop vreviesed are ' num2str(size(vrevised)) ])
    end
   % disp(['The dimensions of vreviesed are ' num2str(size(vrevised)) ])
     diff=norm(vrevised-vinitial)/norm(vrevised); 
     vinitial=vrevised;
    
end

% compute decision rule

derule=zeros(N,Z);

for i=1:Z
    derule(:,i)=x(decision(:,i))';
end

% plot 
% Z = 5 grids 
%v1=vinitial(:,2) ;  % p=0.8268  
%v2=vinitial(:,(Z+1)/2) ; % p=1
%v3=vinitial(:,4) ;  % p=1.1732

% Z = 21 grids 
v1=vinitial(:,8) ;  % value at p=0.9 
v2=vinitial(:,11) ; % p=1
v3=vinitial(:,14) ;  % p=1.1

d1 = derule(:,1) ;  % number of lumber stock at p min 
d2 = derule(:,(Z+1)/2) ; % at p = 1 
d3 = derule(:,Z) ;  % at p max 

figure(1)
plot(x,v1,'-r') 
hold on 
plot(x,v2,'-b')
hold on
plot(x,v3,'-g') 
legend('p=0.83', 'p=1', 'p=1.17')   % Z = 5 
%legend('p=0.9', 'p=1', 'p=1.1')    % Z = 21
xlabel('number of stock')
ylabel('value of the firm')

figure(2)
plot(x,d1,'-r') 
hold on 
plot(x,d2,'-b')
hold on
plot(x,d3,'-g')
hold on
legend('p min', 'p=1', 'p max')
xlabel('number of stock')
ylabel('number of next period stock')

%% Simulation 
% current stock is 100, and current p = 1 
% simulate M many economies 

M = 1000 ; % 1000 simulation 
T = 20 ; % length of simulation 

inp = (Z+1)/2 ; % initial p = 1 
inx = 101 ;     % initial x = 100 
stock = zeros(M,T) ;

for j = 1:M 
    
    inp = (Z+1)/2 ; % initial p = 1 
    inx = 101 ;     % initial x = 100 
    
    for i = 1:T 
        shock = rand(1,1) ;      
        p_prob = prob(inp,:) ;  % take p1 distribution 
        cdf = 0 ; 
    
        for k = 1 : Z       % find p1 
            cdf = cdf + p_prob(k); 
            
            if shock <= cdf
                newinp = k ;    % p1=grid(k) 
                    break      % breaking the loop 
            end
            
        end 
        inx = derule(inx,newinp)+1 ; 
        inp = newinp ;
        stock(j,i) = inx - 1 ; 
    end
end 

stock=sort(stock) ; 
q5 = M*0.05 ; 
q95 = M*0.95 ; 
stock5 = stock(q5,:) ; 
stock95 = stock(q95,:) ; 
meanstock = mean(stock) ; 
xaxis = 1:T ;

figure(3) 
plot(xaxis, stock5,'--r') ; 
hold on 
plot(xaxis, meanstock,'-b') ; 
hold on 
plot(xaxis, stock95,'--r') ; 
xlabel('period')
ylabel('number of stock')



