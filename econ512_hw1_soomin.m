% Econ512 hw#1 written by Soomin Jung %
clear; 
clc; 

% q1 
X = [1 1.5 3.5 4.78 5 7.2 9 10 10.1 10.56];
Y1 = -2 + (0.5 * X);
Y2 = -2 + (0.5 * X.^2); 

plot(X, Y1, 'bo',X, Y2, 'ro')
legend('Y1', 'Y2')
xlabel('X')
ylabel('Y')
 
% q2
T = 120;
k = (40-(-20))/(T-1);
Xq2 = -20:k:40;
sumXq2 = Xq2*ones(T,1);
disp('Q2')
disp('The sum of the vector created is')
disp(sumXq2); 

% q3
Aq3 = [3 4 5; 2 12 6; 1 7 4]; 
b = [3; -2; 10];
C = Aq3'*b;
D = inv(Aq3'*Aq3)*b;
E = ones(1,3)*Aq3*b; 
% should have been Aq3'*b 
F = [1 0 0; 0 0 1]*Aq3*[eye(2); 0 0];
G = inv(Aq3)*b;
disp('Q3')
C
D
E
F
disp('The solution x is')
G

% q4 
H = eye(5);
I = kron(H, Aq3);
disp('Q4')
I 

% q5
clear; 
A = 6.5+3.*randn([5,3]);
B = A>=4 ;  % Bq5 = 0.5*(sign(Aq5-4)+1); 
disp('Q5')
A
B

% q6 
clear;

% import data  
% id 99840 92 data was missing. So I exclude the data from regression. 

data=csvread('datahw1.csv');
% comment from TA: csvread replaces NaN with 0 which makes your dataset corrupted. use readtable instead.
data=data([1:2590,2592:end],:); 
prd = data(:,5); 

k = length(prd);
one = ones([k,1]); 
export = data(:,3);
rd = data(:,4);
cap = data(:,6); 

x = [one export rd cap] ;  
beta = (x'*x)^-1*(x'*prd); 
disp('Q6')
beta 
