function [ obj ] = MClikelihood(x,y,z,beta,gamma,sigmab,u, sigmaub,sigmau)
% Likelihood using Gaussian Quadrature integration 
%beta0, gamma0, sigmab, u0, sigmaub,sigmau
if nargin < 7
    like = 0;

for i=1:100; 
    b = beta+sigmab*randn(1,20);
    % comment from TA: here you redraw every time likelihood is evaluated.
    % that givs hard time for the optimizer and it cannot make a step
    xx=x(:,i);
    yy=y(:,i);
    zz=z(:,i);

    for j=1:20 
        e=b(j)*xx+gamma*zz;
        fe = 1./(1+exp(-e));
        l_1=yy.*log(fe);
        l_0=(1-yy).*log(1-fe);
        l=min(l_1,l_0);         % to prevent -inf 
        like=sum(l)/20+like; 
    end
end 
    obj=-like;
end


if nargin > 6 
    sigma=[sigmab sigmaub; sigmaub sigmau]; 
    like=0;
    mu=[beta;u];
    [~,p]=chol(sigma);
    if p==0
    for i=1:100; 
        R = mvnrnd(mu,sigma,20); 
        xx=x(:,i);
        yy=y(:,i);
        zz=z(:,i);
        
        for j=1:20 
            e=R(j,1)*xx+gamma*zz+R(j,2);
            fe = 1./(1+exp(-e)); 
            l_1=yy.*log(fe);
            l_0=(1-yy).*log(1-fe);
            l=min(l_1,l_0);         % to prevent -inf 
            like=sum(l)/20+like; 
        end
    end
    else
        like=-100000000;
    end 
    
        obj=-like;
end
        
end 

