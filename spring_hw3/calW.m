function [W] = calW(V)
% Learing by duopoly doing 
% Caculate expected future value 
% W0 labeled 1 - (q1=0,q2=0)
% W1 labeled 2 - (q1=1,q2=0)
% W2 labeled 3 - (q1=0,q2=1)

global L delta w

Wn = zeros(L,L,3);
V20 = [V(:,1) V]; % duplicate 1st column of V
V21 = [V V(:,L)]; % duplicate last column of V

% Calculate W0 
for i = 1:L
    if i == 1
        Wn(i,:,1) = ((1-delta).^w).*V20(i,2:L+1)+...
            (1-(1-delta).^w).*V20(i,1:L);
    else
        Wn(i,:,1) = (1-delta)^i*((1-delta).^w).*V20(i,2:L+1)+...
            (1-delta)^i*(1-(1-delta).^w).*V20(i,1:L)+...
            (1-(1-delta)^i)*((1-delta).^w).*V20(i-1,2:L+1)+...
            (1-(1-delta)^i)*(1-(1-delta).^w).*V20(i-1,1:L);
    end
end

% Calculate W1 
for i = 1:L
    if i == L
        Wn(i,:,2) = ((1-delta).^w).*V20(i,2:L+1)+...
            (1-(1-delta).^w).*V20(i,1:L);
    else
        Wn(i,:,2) = (1-delta)^i*((1-delta).^w).*V20(i+1,2:L+1)+...
            (1-delta)^i*(1-(1-delta).^w).*V20(i+1,1:L)+...
            (1-(1-delta)^i)*((1-delta).^w).*V20(i,2:L+1)+...
            (1-(1-delta)^i)*(1-(1-delta).^w).*V20(i,1:L);
    end
end

% Calculate W2 
for i = 1:L
    if i == 1
        Wn(i,:,3) = ((1-delta).^w).*V21(i,2:L+1)+...
            (1-(1-delta).^w).*V21(i,1:L);
    else
        Wn(i,:,3) = (1-delta)^i*((1-delta).^w).*V21(i,2:L+1)+...
            (1-delta)^i*(1-(1-delta).^w).*V21(i,1:L)+...
            (1-(1-delta)^i)*((1-delta).^w).*V21(i-1,2:L+1)+...
            (1-(1-delta)^i)*(1-(1-delta).^w).*V21(i-1,1:L);
    end
end

W = Wn;