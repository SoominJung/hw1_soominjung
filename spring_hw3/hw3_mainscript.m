% Econ 512 2017 Spring hw3 %%% 
% Written by Soomin Jung %%%
% Learning by doing duopoly 
% Besanko, Doraszelski Kryukov, Satterthwaite (2010, ECMA)

clc ; 
clear ; 

% Paremeter 

global L rho kappa l v delta beta eta 
L = 30 ; % number of state (knowledge level)
rho = 0.85 ; % cost parameter 
kappa = 10 ; % minimum knowledge 
l = 15 ; % cost flatten out 
v = 10 ; % quality of good 
delta = 0.03 ; % depreciation probability 
beta = 1/1.05 ; % discounting factor 
eta = log(rho) / log(2) ; 


% Exercise 1 
% compute and plot the value and policy function of an MPE 
 
%% VFI 
V = zeros(L,L) ;    % inital value V 
V1 = zeros(L,L) ;   % Store updated V 
P = v*ones(L,L) ;   % inital P
P1 = zeros(L,L) ;   % updated P 

tol = 1e-4 ; 
T = 1000 ; 
lambda = 0.9; % Dampening ;
w = 1:L ;  
w = w';
diff = 1;

% pr0 = know_trans(0) ; 
% pr1 = know_trans(1) ; 

c = kappa *(w.^eta) ; 
c(l:L,:) = kappa * (l.^eta) ; % cost 

%W0 = zeros(L,L) ; 
%W1 = zeros(L,L) ;
%W2 = zeros(L,L) ; 
iter = 1 ; 
lb = 0.5*c;
%% 

tic 

while diff>tol && iter<T
    for i = 1:30 
      
         W = calW(V) ; % Calculate W 
         
        % Find optimal price 
    
        q = P(:,i);     
        pinitial = P(i,:)';
        ci = c(i);
        w0 = W(i,:,1)';
        w1 = W(i,:,2)';
        w2 = W(i,:,3)'; 
        
        pnew = fsolve(@(x) foc( x,q,ci,w0,w1,w2 ),pinitial,...
            optimoptions('fsolve','algorithm','Levenberg-Marquardt','Display','off'));
       P1(i,:) = pnew' ;
        
        
        % value function update 
        d = demand(pnew,q) ;
        V1(i,:) = d(2,:).*(pnew-ci)' + ...
            beta*(d(1,:).*w0'+d(2,:).*w1'+d(3,:).*w2'); 
    end 
    
    % check convergence 
    d1 = norm((V-V1)./(1+abs(V1))) ; 
    d2 = norm((P-P1)./(1+abs(P1))) ; 
    diff = max(d1,d2);
    iter = iter+1;

            V = lambda*V1+(1-lambda)*V ; 
            P = lambda*P1+(1-lambda)*P ; 

end 

toc
%% Exercise 1 : Plotting  

figure(1)
mesh(V);
alpha(.5)
title('Value Function');
xlabel('w1'); ylabel('w2');zlabel('V(w)');

figure(2)
mesh(P);
alpha(.5)
title('Policy Function');
xlabel('w1'); ylabel('w2');zlabel('p(w)');

%% Exercise 2 : Simulation 
N =10000 ; 
T = 10; 


prob10 = simstate(N,10,P); 
prob20 = simstate(N,20,P);
prob30 = simstate(N,30,P);
%%

x = 1:1:30 ; 
y = 1:1:30 ;
figure(3) 
mesh(x,y,prob10)
alpha(.5)
title('Simulation 10 period')
xlabel('w1'); ylabel('w2');zlabel('Probability(w)');

figure(4)
mesh(x,y,prob20)
alpha(.5)
title('Simulation 20 period')
xlabel('w1'); ylabel('w2');zlabel('Probability(w)');

figure(5)
mesh(x,y,prob30)
alpha(.5)
title('Simulation 30 period')
xlabel('w1'); ylabel('w2');zlabel('Probability(w)');

%% Exercise 3 : stationary distribution 

dist0 = prob30 ; % initial guess 














