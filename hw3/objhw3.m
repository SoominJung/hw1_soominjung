function [ obj,dobj,s ] = objhw3(x,y,b)
% log likelihood function of poisson distribution
% minimized value 
obj=-sum(-exp(x*b)+(y.*(x*b))-log(factorial(y))); 

[n,k]=size(x);
    
if nargout > 1 
    dobj=-sum(diag(-exp(x*b))*x+(diag(y)*x)); 
end 

if nargout>2 
    s=(diag(-exp(x*b))*x+(diag(y)*x))'*(diag(-exp(x*b))*x+(diag(y)*x));
    
end
end