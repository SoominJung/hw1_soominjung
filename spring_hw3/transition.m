function [ d0,d1,d2,delta1,delta2 ] = transition( w1,w2,P )
% Learning by doing 
% transition probability for stionary distribution derivation 
global delta 
    p = P(w1,w2); 
    q = P(w2,w1) ; 
    d = demand(p,q) ; 
    d0 = d(1) ; % q1=q2=0  
    d1 = d(2) ; % q1=1, q2=0 
    d2 = d(3) ; % q1=0, q2=1
    
    delta1 = 1-((1-delta)^w1) ; 
    delta2 = 1-((1-delta)^w2) ;
end

