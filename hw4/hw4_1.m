

% Econ512 hw4 written by Soomin Jung



cd ('C:\Users\Soomin Jung\Documents\Courses\512hw\hw4')
load('hw4data.mat')
diary on

x=data.X;
y=data.Y;
z=data.Z;

%%%%%%% Q1 Gausian Quadrature using 5 nodes %%%%%%% 
beta0=0.1; 
sigmabeta=1; 
gamma=0; 
u=0;

[grid,weight]=qnwnorm(5,beta0,sigmabeta); %% Find nods to estimate

f = 0 ;
g = 0 ; 
like=0; 

for i = 1:5; 
    e=grid(i)*x;
    fe = 1./(1+exp(-e));
    l_1=y.*log(fe);
    % comment from TA: here you are integrating log of conditional
    % likelihood, whereas you really must integrate out conditional
    % likelihood and then multiply if over periods of time for the same
    % individual and only then take a log of that.
    l_0=(1-y).*log(1-fe);
    l=min(l_1,l_0);         % to prevent -inf 
    % comment from TA: this is not consistent with what you are trying to
    % do here, you needed to replicate functional form of likelihood
    % exactly as given in text
    like=sum(sum(l))*weight(i)+like; 

end

like1=sum(like);

disp('Result (Soomin Jung)') 
disp('1. Log likelihood function value') 
disp(like1) 

%%%%%%% Q2 Gausian Quadrature using 10 nodes %%%%%%% 
[grid,weight]=qnwnorm(10,beta0,sigmabeta); %% Find nods to estimate

like = 0;

for i=1:10; 
    e=grid(i)*x;
    fe = 1./(1+exp(-e));
    l_1=y.*log(fe);
    l_0=(1-y).*log(1-fe);
    l=min(l_1,l_0);         % to prevent -inf 
    like=sum(sum(l))*weight(i)+like; 
    end 
 

like2=like;

disp('2. Log likelihood function value') 
disp(like2) 

%%
%%%%%%% Q3 Montel Carlow Methods using 20 nodes %%%%%%% 
i=1; 

like3=MClikelihood(x,y,z,beta0,sigmabeta,gamma);

disp('3. Log ikelihood function value')
disp(-like3)

%%
%%%%%%% Q4 MLE estimation %%%%%%%     

% Gaussian Quadrature using 5 nodes%
p1=5; 
guess=[0.1,0,1];
A=[0 0 0; 0 0 0; 0 0 -1];
B=[0;0;0];
obj = @(para) GQlikelihood(x,y,z,para(1),para(2),para(3),p1); 

options = optimset('Display','iter','algorithm','interior-point','MaxFunEvals',5000,'MaxIter',5000);
[beta1, like1, ~,out] = fmincon(obj,guess, A, B ,[],[],[],[],[],options);

% Comment from TA: your estimate of zero beta variance is a consequence of
% your incorrect integration 
disp('MLE estimation') 
disp('1. Gaussian Quadrature using 5 nodes')
disp('starting value')
guess
disp('estimate')
beta1
disp('fval')
-like1
out 
% Gaussian Quadrature using 10 nodes%

p2=10; 
obj = @(para) GQlikelihood(x,y,z,para(1),para(2),para(3),p2); 
[beta2, like2, ~, out] = fmincon(obj, guess, A, B,[],[],[],[],[],options);

disp('2. Gaussian Quadrature using 10 nodes')
disp('starting value')
guess
disp('estimate')
beta2
disp('fval')
-like2
out 
% Montel Carlow Methods using 20 nodes %    
    
obj = @(para) MClikelihood(x,y,z,para(1),para(2),para(3)); 
[beta3, like3,~,out] = fmincon(obj,guess, A, B ,[],[],[],[],[],options);
% Comment from TA: here if you turn on your display optinon as I did for
% you, you will see that optimization actually stuck because  you redraw MC
% every iteration. To prevent that you need to fix seed, see how to da that
% in answer key
disp('3. Montel Carlow Methods using 20 nodes')
disp('starting value')
guess
disp('estimate')
beta3
disp('fval')
-like3
out 

%%
% Montel Carlow Methods u =~0%    
guess=[beta3' ; 0.1; 1; 1]; %beta0, gamma0, sigmab, u0, sigmaub,sigmau 
A=[0 0 0 0  0 0;0 0 0 0 0 0 ;0 0 -1 0 0 0;0 0 0 0 0 0;...
   0 0 0 0 0 0;0 0 0 0 0 -1];
B=[0;0;0;0;0;0];
options = optimset('Display','off','algorithm','interior-point','MaxFunEvals',5000,'MaxIter',5000);
obj = @(para) MClikelihood(x,y,z,para(1),para(2),para(3),para(4),para(5),para(6)); 
[beta4, like4,~,out] = fmincon(obj,guess, A, B ,[],[],[],[],[],options);

disp('MLE with u=~0')
disp('Montel Carlow Methods using 20 nodes')
disp('starting value 1')
guess
disp('estimate')
beta4
disp('fval')
-like4
out

guess=beta4; %beta0, gamma0, sigmab, u0, sigmaub,sigmau 
options = optimset('Display','off','algorithm','interior-point','MaxFunEvals',5000,'MaxIter',5000);
nonlcon=@covpd;
obj = @(para) MClikelihood(x,y,z,para(1),para(2),para(3),para(4),para(5),para(6)); 
[beta5, like5,~,out] = fmincon(obj,guess, A, B,[],[],[],[],[],options);

disp('starting value 2 ')
guess
disp('estimate')
beta5
disp('fval')
-like5
out

guess=beta5; %beta0, gamma0, sigmab, u0, sigmaub,sigmau 
options = optimset('Display','off','algorithm','interior-point','MaxFunEvals',5000,'MaxIter',5000);
nonlcon=@covpd;
obj = @(para) MClikelihood(x,y,z,para(1),para(2),para(3),para(4),para(5),para(6)); 
[beta6, like6,~,out] = fmincon(obj,guess, A, B ,[],[],[],[],[],options);

disp('starting value 3 ')
guess
disp('estimate')
beta6
disp('fval')
-like6
out

diary off