%Econ512 hw2 written by Soomin Jung
clc
clear all

%Quantity 
qa=@(v,p)exp(v(1)-p(1))/(1+exp(v(1)-p(1))+exp(v(2)-p(2))+exp(v(3)-p(3)));
qb=@(v,p)exp(v(2)-p(2))/(1+exp(v(1)-p(1))+exp(v(2)-p(2))+exp(v(3)-p(3)));
qc=@(v,p)exp(v(3)-p(3))/(1+exp(v(1)-p(1))+exp(v(2)-p(2))+exp(v(3)-p(3)));

%1 
v1=[-1;-1;-1];
p1=[1;1;1];
qa1=qa(v1,p1); 
qb1=qb(v1,p1);
qc1=qc(v1,p1); 
q01=1-(qa1+qb1+qc1);

disp('Question1')
disp('qa  qb  qc  q0')
[qa1,qb1,qc1,q01]

% 2 Broyden 
pmat=[1,1,1;0,0,0;0,1,2;3,2,1]; 
result2=zeros(3,4); 
tol=0.0000001 ;

optset('broyden','tol',tol);
tic
for i=1:4 
    p=pmat(i,:)';
    [eqp,fval,flag,it,~]=broyden('sysfoc',p,v1);
    result2(:,i)=eqp;   
end
toc

disp('Question2')
disp('Each column corresponds to each starting value')
result2 

% 3  Gauss-Siedel 
v1=[-1;-1;-1];
pmat=[1,1,1;0,0,0;0,1,2;3,2,1]; 
T=100; 
result3=zeros(3,4);  


f=@(p)(1-p.*(1-(exp(v1-p)/(1+sum(exp(v1-p)))))); %foc 
% Comment from TA: you have defined foc for firm 1 and used that for all
% functions, even though you should have had 3 different FOCs
tic 
for i=1:4 
    p0=pmat(i,:)';
    d=1; 
    k=1; 
    h=1;
    g=1;
    l=1;
     
    p1=p0+0.1; %Second guess 
    
    while d > tol % && k<100 

        f0=f(p0); 
        fa0=f0(1);
        f1=f(p1);
        fa1=f1(1); 
        newpa=p1(1)-fa1*(p1(1)-p0(1))/(fa1-fa0);
        newp=[newpa;p1(2:3)]; 
        p0=p1;
        p1=newp;
        
             %pa loop 
         for g=1:2   
 
         f0=f(p0);
         fa0=f0(1);
         f1=f(p1);
         fa1=f1(1); 
             if abs(p0(1)-p1(1))<tol
                 g=2;
             else
         newpa=p1(1)-((p1(1)-p0(1))/(fa1-fa0)*fa1);
         newp=[newpa;p1(2:3)];
             end
         p0=p1;
         p1=newp; 
         end
 
    
        %pb loop 
        for h=1:3

        f0=f(p0);   
        fb0=f0(2);
        f1=f(p1);
        fb1=f1(2); 
            if abs(fb1-fb0)<tol
                h=3;
            else 
        newpb=p1(2)-((p1(2)-p0(2))/(fb1-fb0)*fb1);
        newp=[newpa;newpb;p1(3)]; 
        p0=p1;
        p1=newp;
        h=h+1;
            end
        end
      
        %pc loop 
        for l=1:2 
        f0=f(p0);  % Here should have been different function, see answer key
        fc0=f0(3);
        f1=f(p1);
        fc1=f1(3); 
            if abs(p1(3)-p0(3))<tol
                    l=2;    
            else 
        newpc=p1(3)-(p1(3)-p0(3))/(fc1-fc0)*fc1;
        newp=[p1(1:2);newpc]; 
        p0=p1;
        p1=newp;
        l=l+1; 
        end
        end
        
 
        d=sum(abs(p0-p1)); 
        p0=p1; 
        p1=newp; 
        k=k+1; 
     
    end 
    result3(:,i)=p1'; 
end
toc 

disp('Question3')
disp('Each column corresponds to each starting value')

result3 

% 4 New updating rule 
% p^{t+1}=1/(1-D(p^{t}))
 
result4=zeros(3,4);

tic
for i=1:4 
    p=pmat(i,:)';
    d=1; 
    k=1; 

    while d>tol && k<100
    qa=@(v1,p)exp(v1(1)-p(1))/(1+sum(exp(v1-p)));
    qb=@(v1,p)exp(v1(2)-p(2))/(1+sum(exp(v1-p)));
    qc=@(v1,p)exp(v1(3)-p(3))/(1+sum(exp(v1-p)));
    q=[qa(v1,p);qb(v1,p);qc(v1,p)]; 
    newp=1./(1-q); 
    d=sum(abs(p-newp)); 
    p=newp; 
    k=k+1; 
    end 
    result4(:,i)=p;
end 
toc 

disp('Question4')
disp('Each column corresponds to each starting value')
result4 

% Question 5 
% Va=Vb=-1, Vc=-4:1

p=[1;1;1]; 
result5=zeros(6,3); 
vcvec=-4:1; 
for i=1:6
    vc=vcvec(i);
    v=[-1,-1,vc]; 
    [eqp,fval,flag,it,~]=broyden('sysfoc',p,v);
    result5(i,:)=eqp';  
end

pa=result5(:,1); 
pc=result5(:,3); 

plot(vcvec,pa,'r-o',...
    'Markerfacecolor','r',...
    'markersize',4)

hold on
plot(vcvec,pc,'b-o',...
    'Markerfacecolor','b',...
    'markersize',4) 
legend('Price a','Price c');
xlabel('Vc')
ylabel('price')
