function [ output ] = profit( p,q,c,w0,w1,w2 )
% Learing by duopoly doing 
% profit 
% fix firm 1's state as w(i)
% p is firm 1's price firm 2's state given) 
% q is frim 2's price (Lx1) for each state w 

global L rho kappa l v delta beta 

 
d = demand(p,q);
margin = p - c;
profit = d(2,:)'.*margin + beta*(d(1,:)'.*w0+d(2,:)'.*w1+d(3,:)'.*w2); 
output = sum(profit.^2);

end

